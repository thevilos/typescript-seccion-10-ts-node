import powers from "../data/powers";
import { Power } from '../data/powers';

export class Hero {
  constructor(
    public name: string,
    public powerId: number,
    public age: number,
  ) {}

  get power() {
    return powers.find((power: Power)  => power.id === this.powerId)?.des || 'not found';
  }
}

export class Hero2 {}
export class Hero3 {}
export class Hero4 {}

export const PI = 3.1416;
export const miNombre = 'Edgard';