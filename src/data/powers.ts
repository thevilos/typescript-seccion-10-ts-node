export interface Power {
  id: number;
  des: string;
}

const powers: Power[] = [
  {
    id: 1,
    des: 'Money',
  },
  {
    id: 2,
    des: 'Drugs',
  },
];

export default powers;