define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const powers = [
        {
            id: 1,
            des: 'Money',
        },
        {
            id: 2,
            des: 'Drugs',
        },
    ];
    exports.default = powers;
});
